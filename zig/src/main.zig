const std = @import("std");
const toml = @import("toml");
const log = std.log;
const heap = std.heap;
const mem = std.mem; // believe it or not this is mostly strings

const expect = std.testing.expect;

const I3Connection = @import("i3.zig").I3Connection;

const ChildProcess = std.ChildProcess;
const ArrayList = std.ArrayList;
const StringHashMap = std.StringHashMap;

const stdout = std.io.getStdOut().writer();
const stderr = std.io.getStdErr().writer();

const DeserializationError = error{
    InvalidFieldType,
    MissingField,
    InvalidArgument,
};

const CONFIG_FILE_VAR = "TVU_CONFIG_FILE";

fn configpath(alloc: mem.Allocator) ![]const u8 {
    if (std.os.getenv(CONFIG_FILE_VAR)) |fname| {
        return fname;
    }
    const home = std.os.getenv("HOME") orelse unreachable;
    return try std.fs.path.join(alloc, &[_][]const u8{ home, ".config", "tvu", "config.toml" });
}

fn loadconfig(alloc: mem.Allocator) !*toml.Table {
    const p = try configpath(alloc);
    var o = try toml.parseFile(alloc, p, null);
    return o;
}

fn from_toml_string_array(alloc: mem.Allocator, v: toml.DynamicArray) ![]const []const u8 {
    var o = ArrayList([]const u8).init(alloc);
    // can we be absolutely certain this allocates *ONLY* the underlying array?
    for (v.items) |ss| {
        const s = switch (ss) {
            .String => |s| s,
            else => return DeserializationError.InvalidFieldType,
        };
        try o.append(s);
    }
    return o.items;
}

const Channel = struct {
    //for handling owned ChildProcess
    alloc: mem.Allocator,

    name: ArrayList(u8),

    //this will be owned by the Channel
    cmd: ChildProcess,

    pub fn getname(self: @This()) []const u8 {
        return self.name.items;
    }

    pub fn init(
        alloc: mem.Allocator,
        name: []const u8,
        cmd: []const []const u8,
    ) !Channel {
        var _name = ArrayList(u8).init(alloc);
        try _name.appendSlice(name);
        var _cmd = try ChildProcess.init(cmd, alloc);
        return Channel{
            .alloc = alloc,
            .name = _name,
            .cmd = _cmd.*,
        };
    }

    pub fn deinit(self: *Channel) void {
        self.name.deinit();
        self.cmd.deinit();
    }

    pub fn fromtoml(alloc: mem.Allocator, t: *toml.Table) !Channel {
        const name_ = t.keys.get("name").?;
        const name = switch (name_) {
            .String => |s| s,
            else => return DeserializationError.InvalidFieldType,
        };
        const cmd_ = t.keys.get("cmd").?;
        const cmd = switch (cmd_) {
            .Array => |a| try from_toml_string_array(alloc, a),
            else => return DeserializationError.InvalidFieldType,
        };
        return try Channel.init(alloc, name, cmd);
    }

    pub fn activate(self: @This(), alloc: mem.Allocator, i3cnxn: *I3Connection) !void {
        const s = try std.fmt.allocPrint(alloc, "workspace {s}", .{self.getname()});
        defer alloc.free(s);
        _ = try i3cnxn.cmd_raw(alloc, s);
    }

    pub fn run(self: *@This(), alloc: mem.Allocator, i3cnxn: *I3Connection) !void {
        if (try i3cnxn.has_workspace(alloc, self.getname())) {
            try self.activate(alloc, i3cnxn);
        } else {
            try self.activate(alloc, i3cnxn);
            _ = try self.cmd.spawn();
        }
    }
};

const Config = struct {
    // handles below hash map
    alloc: mem.Allocator,

    channels: StringHashMap(Channel),

    pub fn init(
        alloc: mem.Allocator,
        chs: []Channel,
    ) !Config {
        var hm = StringHashMap(Channel).init(alloc);
        for (chs) |ch| {
            try hm.put(ch.getname(), ch);
        }
        return Config{
            .alloc = alloc,
            .channels = hm,
        };
    }

    pub fn get(self: @This(), k: []const u8) ?*Channel {
        return self.channels.getPtr(k);
    }

    pub fn deinit(self: *Config) void {
        self.channels.deinit();
    }

    pub fn fromtoml(alloc: mem.Allocator, t: *toml.Table) !Config {
        var chs = ArrayList(Channel).init(alloc);
        defer chs.deinit();
        const ft = t.keys.get("channels") orelse return DeserializationError.MissingField;
        switch (ft) {
            .ManyTables => |mt| for (mt.items) |ch| {
                try chs.append(try Channel.fromtoml(alloc, ch));
            },
            else => return DeserializationError.InvalidFieldType,
        }
        return Config.init(alloc, chs.items);
    }

    pub fn from_default_toml(alloc: mem.Allocator) !Config {
        var t = try loadconfig(alloc);
        defer t.deinit();
        return fromtoml(alloc, t);
    }

    pub fn list(self: @This()) !void {
        var itr = self.channels.keyIterator();
        while (itr.next()) |ch| {
            try stdout.print("{s}\n", .{ch.*});
        }
    }

    pub fn run(self: *Config, alloc: mem.Allocator, i3cnxn: *I3Connection, name: []const u8) !?*Channel {
        var ch = self.get(name) orelse return null;
        try ch.run(alloc, i3cnxn);
        return ch;
    }
};

fn print_help() !void {
    return stderr.print(
        \\tvu
        \\
        \\TV control utility.  Requires config file, set location with `TVU_CONFIG_FILE`,
        \\defaults to `$HOME/.config/tvu/config.toml`.
        \\
        \\USAGE:
        \\  tvu [SUBCOMMAND]
        \\
        \\SUBCOMMANDS:
        \\  help            Print this message.
        \\  ls              list available channels
        \\  run             run a channel, requires channel argument
    , .{});
}

pub fn main() !void {
    var arena = heap.ArenaAllocator.init(heap.page_allocator);
    defer arena.deinit(); // this should obviate the need for all other deinit
    const alloc = arena.allocator();

    var args = std.process.args();

    // first arg is always exename
    _ = try args.next(alloc).?;

    // subcmd needs some special handling to delay toml parsing
    const subcmd = try args.next(alloc) orelse {
        try stderr.print("tvu: requires subcommand", .{});
        try print_help();
        return error.InvalidArgument;
    };

    if (mem.startsWith(u8, subcmd, "-")) {
        try stderr.print("tvu: first argument must be subcommand\n", .{});
        try print_help();
    }

    //load toml
    var cfg = try Config.from_default_toml(alloc);
    var i3cnxn = try I3Connection.init(alloc);

    if (mem.eql(u8, subcmd, "help")) {
        try print_help();
    } else if (mem.eql(u8, subcmd, "ls")) {
        try cfg.list();
    } else if (mem.eql(u8, subcmd, "run")) {
        const chstr = try args.next(alloc) orelse {
            try stderr.print("tvu run: requires channel name argument", .{});
            return error.InvalidArgument;
        };
        _ = (try cfg.run(alloc, &i3cnxn, chstr)) orelse {
            try stderr.print("tvu run: channel not found ({s})", .{chstr});
            return error.InvalidArgument;
        };
    } else {
        try stderr.print("tvu: missing or invalid subcommand argument\n", .{});
        try print_help();
    }
}
