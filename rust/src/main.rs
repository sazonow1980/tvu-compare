#![allow(dead_code)]
#![allow(unused)]

mod utils;

use std::io::prelude::*;
use std::fs::File;
use std::io;
use std::fmt;
use std::env;
use std::error::Error;
use std::path::PathBuf;
use std::process::Command;
use std::borrow::Borrow;

use anyhow::{Context, Result};

use indexmap::IndexMap; //ordered dict, for preserving order in config file

use clap::{Arg, App, SubCommand};

use i3_ipc::{Connect, I3, I3Stream};
use i3_ipc::reply;

use serde::{Serialize, Deserialize};

use toml;


const CONFIG_FILE_VAR: &str = "TVU_CONFIG_FILE";

const ABOUT_TEXT: &str = "TV control utility. Requires config file, set location with `TVU_CONFIG_FILE`, defaults \
                          to `$HOME/.config/tvu/config.toml`";


fn has_workspace(i3: &mut I3Stream, ws: &str) -> Result<bool, io::Error> {
    for w in i3.get_workspaces()? {
        Some(w.name) == Some(ws.to_string()) && return Ok(true);
    }
    Ok(false)
}

// custom serde implementations are a gigantic pain in the ass, this is easier
#[derive(Debug,Deserialize,Serialize)]
struct TomlChannel {
    name: String,

    //NOTE: it's bad for this to have Command which it owns because it requires that
    //either cmd is copied or this object must exist in perpetuity to be referred to
    //not sure what the best solution for this is...
    //serde is definitely not doing me any favors here
    cmd: Vec<String>,
}


#[derive(Debug)]
struct Channel {
    name: String,
    cmd: Command,
}

impl Channel {
    fn new_from_strings(name: &str, v: Vec<String>) -> Self {
        Channel {name: name.into(), cmd: utils::command_from_strings(v.into_iter())}
    }

    fn new_from_string(name: &str, s: String) -> Self {
        Channel {name: name.into(), cmd: utils::command_from_string(s)}
    }

    fn from_toml(τ: TomlChannel) -> Self {
        Self::new_from_strings(&τ.name, τ.cmd)
    }

    fn activate(&self, i3: &mut I3Stream) -> io::Result<Vec<reply::Success>> {
        i3.run_command(format!("workspace {}", self.name))
    }

    fn run(&mut self, i3: &mut I3Stream) -> io::Result<Vec<reply::Success>> {
        has_workspace(i3, &self.name)? && return self.activate(i3);
        let o = self.activate(i3);
        self.cmd.spawn();
        o
    }
}

#[derive(Debug,Deserialize,Serialize)]
struct TomlConfig {
    channels: Vec<TomlChannel>,
}

#[derive(Debug)]
struct Config {
    //NOTE: here is a really unpleasant case;
    //would really like to have the keys be a &str to the names owned
    //by the values, but the problem is that we would have to do this after the
    //fact, it's impossible to return the value from a function and have the
    //compiler know that it will live long enough to be used as the key
    channels: IndexMap<String, Channel>,
}

impl Config {
    fn from_toml(τ: TomlConfig) -> Self {
        let f = |χ| {
            let mut o = Channel::from_toml(χ);
            // here's what I was referring to above; we are stuck having
            // to copy instead of just making a reference
            (o.name.clone(), o)
        };
        let iter = τ.channels.into_iter();
        let h = IndexMap::from_iter(iter.map(f));
        Config {channels: h}
    }

    fn list(self) -> Vec<String> { self.channels.keys().map(|s| s.clone()).collect() }

    fn run(&mut self, i3: &mut I3Stream, name: &str) -> io::Result<Vec<reply::Success>> {
        match self.channels.get_mut(name) {
            Some(χ) => χ.run(i3),
            None => Err(io::Error::new(io::ErrorKind::Other,
                        format!("could not find channel with name: '{}'", name))),
        }
    }
}

fn default_app<'a>() -> App<'a> {
    let ls = App::new("ls").about("list available channels");

    let run = App::new("run").about("run a channel")
        .arg(Arg::new("CHANNEL").about("channel to run").required(true).index(1));

    let app = App::new("tvu").about(ABOUT_TEXT)
        .subcommand(ls).subcommand(run);

    app
}

fn configpath() -> PathBuf {
    let v = env::var(CONFIG_FILE_VAR);
    if v.is_ok() {
        PathBuf::from(v.unwrap())
    } else {
        [env::var("HOME").unwrap().as_str(), ".config", "tvu", "config.toml"].iter().collect()
    }
}

fn loadconfig() -> Result<TomlConfig> {
    let mut f = File::open(configpath())?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    let o: TomlConfig = toml::from_str(&s)?;
    Ok(o)
}

fn main() -> Result<()> {
    //this happens first so we don't get errors if all we tried was --help
    let args = default_app().get_matches();

    let mut i3 = I3::connect()?;

    let τ = loadconfig().context("failed to find config file")?;
    let mut cfg = Config::from_toml(τ);

    if args.subcommand_matches("ls").is_some() {
        let ls = cfg.list();
        //iter().map(f) doesn't work here, fuck if I know why
        for s in ls { println!("{}", s); }
    } else if let Some(mrun) = args.subcommand_matches("run") {
        //clap already ensures this is required so we are free to unwrap
        let χ = mrun.value_of("CHANNEL").unwrap();
        cfg.run(&mut i3, χ);
    }

    Ok(())
}
